"use strict";

HavaDurumu.controller("CityAddController", function ($scope, ServiceProvider, WSProvider) {
    $scope.cityListJson = [];

    $scope.searchCity = function () {
        ServiceProvider.SearchCity($scope.citySearchValue, "citySearchList", "handlerCityData");
    };
    
    $scope.handlerCityData = function (data) {
        $scope.cityListJson = data.search_api.result;
    };
    
    $scope.selectedCity = function (item) {
        console.log(item);
        ServiceProvider.GetCityWeather(item.areaName[0].value + "," + item.region[0].value, "cityListContainer", "handlerCityWeatherData");
    };
});