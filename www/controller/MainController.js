"use strict";

HavaDurumu.controller("MainController", function($scope, $rootScope, CityService, $ionicPopup, $state) {
    $scope.thirdDayName = getTRDayName(new Date().addDays(2).toFormat("DDDD"));
    $scope.stateName = $state.current.name;

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        $scope.stateName = toState.name;

        if (toState.name == "app.cityAdd" && CityService.getCityList().length == 5) {
            event.preventDefault();
            $ionicPopup.alert({
                title: 'Uyarı',
                template: "En fazla 5 şehir ekleyebilirsiniz.",
                buttons: [{
                        text: 'Tamam',
                        type: 'button-positive'
                }]
            });
        }
    });
});
