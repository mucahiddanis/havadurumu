"use strict";

var HavaDurumuKeys = {
    worldWeatherOnlineWSKey: {
        url: "http://api.worldweatheronline.com/free/v2/weather.ashx",
        apiKey: "fe0f58ab67be229c941b83a8afbdb",
        lang: "tr",
        showlocaltime: "yes",
        format: "json",
        num_of_days: 3
    },
    cityList: "HavaDurumuCityList",
    selectedCity: "HavaDurumuSelectedCity"
};
