"use strict";

HavaDurumu.service("ServiceProvider", function (WSProvider) {
   return {
       SearchCity: function (searchValue, elementId, callBackMethodName) {
           WSProvider.execute("http://api.worldweatheronline.com/free/v2/search.ashx",
               {
                   query: searchValue,
                   key: HavaDurumuKeys.worldWeatherOnlineWSKey.apiKey,
                   format: HavaDurumuKeys.worldWeatherOnlineWSKey.format
               }, elementId, callBackMethodName);
       },

       GetCityWeather: function (cityName, elementId, callBackMethodName) {
           WSProvider.execute("http://api.worldweatheronline.com/free/v2/weather.ashx",
               {
                   q: cityName,
                   key: HavaDurumuKeys.worldWeatherOnlineWSKey.apiKey,
                   lang: HavaDurumuKeys.worldWeatherOnlineWSKey.lang,
                   showlocaltime: HavaDurumuKeys.worldWeatherOnlineWSKey.showlocaltime,
                   num_of_days: HavaDurumuKeys.worldWeatherOnlineWSKey.num_of_days,
                   format: HavaDurumuKeys.worldWeatherOnlineWSKey.format
               }, elementId, callBackMethodName);
       }
   }
});

HavaDurumu.service("WSProvider", function ($http) {
    return {
        execute: function(url, params, elementId, callBackMethodName) {
            $http({
                method: "GET",
                url: url,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                params: params
            })
            .success(function (result, status) {
                console.log(result);
                angular.element(document.getElementById(elementId)).scope()[callBackMethodName](result);
            })
            .error(function (result, status) {
                console.log(result);
            });
        }
    };
});