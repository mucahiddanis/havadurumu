"use strict";

function toMap(array, getKey, scope) {
    var map = {},
        i = array.length;

    if (!getKey) {
        while (i--) {
            map[array[i]] = i+1;
        }
    } else if (typeof getKey === 'string') {
        while (i--) {
            map[array[i][getKey]] = i+1;
        }
    } else {
        while (i--) {
            map[getKey.call(scope, array[i])] = i+1;
        }
    }

    return map;
}

function getTRDayName(engDayName) {
    var dayNames = {
        Sunday: "Pazar",
        Monday: "Pazartesi",
        Tuesday: "Salı",
        Wednesday: "Çarşamba",
        Thursday: "Perşembe",
        Friday: "Cuma",
        Saturday: "Cumartesi"
    };
    return dayNames[engDayName];
}