
var HavaDurumu = angular.module('HavaDurumu', ['ionic'])

.run(function($ionicPlatform, $ionicPopup) {
  $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
          StatusBar.styleDefault();
      }
      document.addEventListener("offline", function () {
          $ionicPopup.alert({
              title: 'Uyarı',
              template: "İnternet Bağlantınızı Açıp Tekrar Deneyin.",
              buttons: [{
                  text: 'Tamam',
                  type: 'button-positive',
                  onTap: function (e) {
                      navigator.app.exitApp();
                  }
              }]
          });
      }, false);
  });
})
.config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider
          .state("app", {
            templateUrl: "view/main.html",
            controller: "MainController",
            abstract: true
          })

          .state("app.nowTab", {
              url: "/nowTab",
              views: {
                  "main-view": {
                      templateUrl: "view/nowTab.html",
                      controller: "TabsController"
                  }
              }
          })

          .state("app.todayTab", {
              url: "/todayTab",
              views: {
                  "main-view": {
                      templateUrl: "view/todayTab.html",
                      controller: "TabsController"
                  }
              }
          })

          .state("app.tomorrowTab", {
              url: "/tomorrowTab",
              views: {
                  "main-view": {
                      templateUrl: "view/tomorrowTab.html",
                      controller: "TabsController"
                  }
              }
          })

        .state("app.thirdDayTab", {
              url: "/thirdDayTab",
              views: {
                  "main-view": {
                      templateUrl: "view/thirdDayTab.html",
                      controller: "TabsController"
                  }
              }
          })

          .state("app.cityList", {
              url: "/cityList",
              views: {
                  "main-view": {
                      templateUrl: "view/cityList.html",
                      controller: "CityListController"
                  }
              }
          })

          .state("app.cityAdd", {
              url: "/cityAdd",
              views: {
                  "main-view": {
                      templateUrl: "view/cityAdd.html",
                      controller: "CityAddController"
                  }
              }
          });
      $urlRouterProvider.otherwise("/nowTab");
});
