"use strict";

HavaDurumu.controller("CityListController", function ($scope, $ionicHistory, CityService, $ionicPopup, ServiceProvider) {
    $scope.cityList = CityService.getCityList();
    if($scope.cityList.length != 0)
        $scope.cityListModel = CityService.getSelectedCity().name;

    $scope.handlerCityWeatherData = function(result) {
        result.data.name = result.data.request[0].query;
        if(CityService.getCityDataByName(result.data.name)) {
            $ionicPopup.alert({
                title: 'Hata',
                template: "Bu şehir zaten listenizde mevcut (" + result.data.name + ")",
                buttons: [{
                    text: 'Tamam',
                    type: 'button-positive'
                }]
            });
            return false;
        }
        $ionicHistory.goBack();
        CityService.addCity(result.data);
        $scope.cityListModel = result.data.name;
        CityService.setSelectedCity(result.data);
    };

    $scope.$watch('cityListModel', function(value) {
        CityService.setSelectedCity(CityService.getCityDataByName(value));
        angular.element(document.getElementById("homeDiv")).scope().city = CityService.getSelectedCity();
    });

    $scope.selectedCityChange = function (cityData) {
        CityService.setSelectedCity(cityData);
        angular.element(document.getElementById("homeDiv")).scope().city = CityService.getSelectedCity();
        ServiceProvider.GetCityWeather(cityData.name, "cityListContainer", "refreshedData");
    };

    $scope.refreshedData = function (result) {
        result.data.name = result.data.request[0].query;
        CityService.setCityData(result.data.name, result.data);
        $scope.cityListModel = result.data.name;
    };
});