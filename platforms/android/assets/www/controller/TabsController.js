"use strict";

HavaDurumu.controller("TabsController", function($scope, $state, CityService, ServiceProvider) {
    $scope.city = CityService.getSelectedCity();

    if(!localStorage.getItem(HavaDurumuKeys.cityList)) {
        $state.go("app.cityList");
    }

    $scope.refreshWeather = function () {
        ServiceProvider.GetCityWeather($scope.city.name, "homeDiv", "refreshedData");
    };

    $scope.refreshedData = function (result) {
        result.data.name = result.data.request[0].query;
        CityService.setCityData(result.data.name, result.data);
    };
});
