"use strict";

HavaDurumu.service("CityService", function () {
   return {
       cityList: JSON.parse(localStorage.getItem(HavaDurumuKeys.cityList)) || [],
       selectedCity: null,

       getCityList: function () {
           return this.cityList;
       },

       addCity: function (cityData) {
           this.cityList.push(cityData);
           localStorage.setItem(HavaDurumuKeys.cityList, JSON.stringify(this.cityList));
       },

       getSelectedCity: function () {
           if(localStorage.getItem(HavaDurumuKeys.selectedCity) != "undefined")
               this.setSelectedCity(JSON.parse(localStorage.getItem(HavaDurumuKeys.selectedCity)));
           return this.selectedCity;
       },

       setSelectedCity: function (city) {
           this.selectedCity = city;
           localStorage.setItem(HavaDurumuKeys.selectedCity, JSON.stringify(this.selectedCity));
       },

       getCityDataByName: function(cityName) {
           var cityMap = toMap(this.cityList, "name");
           return this.cityList[cityMap[cityName] -1];
       },

       getCityIndexByName: function (cityName) {
           var cityMap = toMap(this.cityList, "name");
           return cityMap[cityName] -1;
       },

       setCityData: function (cityName, newCityData) {
           this.cityList[this.getCityIndexByName(cityName)] = newCityData;
       }
   };
});